
public class Wrappers {

	public static void main(String[] args) {
		
		Byte byteB = 120;
		System.out.println(byteB);
		Short shortS = 1;
		System.out.println(shortS);
		Character characterC = 'Z';
		System.out.println(characterC);
		Double doubleD = 12.5d;
		System.out.println(doubleD);
		Float floatF = 5.5f;
		System.out.println(floatF);
		Integer integerI = 10;
		System.out.println(integerI);
		Long longL = 200L;
		System.out.println(longL);
		Boolean booleanB = true;
		System.out.println(booleanB);
		
	}

}
